import argparse
import re
import random as r
import json

def parser():
    p = argparse.ArgumentParser()
    p.add_argument("-i", "--input_server_ini", help="Path to input server.ini file (mandatory)")
    p.add_argument("-o", "--output_server_ini", help="Path to ouput server.ini file (if not given, will use info from input_server_ini flag", default=None)
    p.add_argument("-m", "--map_pool_json", help="Path to json file wih mappool data")
    return p.parse_args()

def retrieve_gameplay_maplist(ini):
    gameplay_maplist_pattern = r"\[Gameplay\.MapList\].*?\["
    gameplay_maplist = re.search(gameplay_maplist_pattern, ini, re.DOTALL)
    return gameplay_maplist.group(0)

def list_maps(string):
    maps_pattern = r"Maps=(\S+)"
    maps = re.findall(maps_pattern, string)
    return maps

def create_repated_map_list(map_pool_data):
    repeating_map_list = []
    for k, v in map_pool_data.items():
        repeating_map_list.extend([k]*v)

    return repeating_map_list

def main():
    args = parser()

    with open(args.input_server_ini) as ini_f, open(args.map_pool_json) as json_f:
        ini_content = ini_f.read()
        map_pool_data = json.load(json_f)

    gameplay_maplist = retrieve_gameplay_maplist(ini_content)
    current_maps = list_maps(gameplay_maplist)
    assert current_maps, "No maps were found!"

    new_maps = create_repated_map_list(map_pool_data)
    while True:
        new_map = r.choice(new_maps)
        if new_map not in current_maps:
            break

    map_to_remove = r.choice(current_maps)

    new_gameplay_maplist = gameplay_maplist.replace(map_to_remove, new_map)
    new_ini_content = ini_content.replace(gameplay_maplist, new_gameplay_maplist)

    with open(args.output_server_ini if args.output_server_ini is not None else args.input_server_ini, "w") as f:
        f.write(new_ini_content)

if __name__ == "__main__":
    main()